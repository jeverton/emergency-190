package com.lme.emergency190;

import java.io.File;
import java.net.URLConnection;

public class Utils {

	public static boolean isEmailValidate(String email) {

		String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

		if (email.trim().matches(emailPattern)) {

			return true;

		} else {

			return false;

		}
	}
	
	public static String getMimeType(File file){
		return URLConnection.guessContentTypeFromName(file.getName());
	}
	
	public static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

}
