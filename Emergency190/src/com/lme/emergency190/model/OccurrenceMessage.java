package com.lme.emergency190.model;

public class OccurrenceMessage {
	
	private Occurrence occurrence;

	public Occurrence getOccurence() {
		return occurrence;
	}

	public void setOccurence(Occurrence occurence) {
		this.occurrence = occurence;
	}

}
