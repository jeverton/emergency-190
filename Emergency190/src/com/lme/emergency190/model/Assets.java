package com.lme.emergency190.model;

import com.google.gson.annotations.SerializedName;

public class Assets {

	private String name;
	private String attachment;
	@SerializedName("mime_type")
	private String mimeType;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	
}
