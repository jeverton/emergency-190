package com.lme.emergency190.model;

import com.google.gson.annotations.SerializedName;

public class Login {
	
	@SerializedName("grant_type")
	private String grantType;
	@SerializedName("client_id")
	private String clientId;
	private String username;
	private String password;
	
	public String getGrantType() {
		return grantType;
	}
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
