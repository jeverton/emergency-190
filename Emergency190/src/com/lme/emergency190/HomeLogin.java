package com.lme.emergency190;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.rest.RestService;
import com.lme.emergency190.model.Login;
import com.lme.emergency190.model.SessionResponse;
import com.lme.emergency190.webservice.APIEmergencyWSClient;

@SuppressLint("NewApi")
@EActivity
public class HomeLogin extends SherlockActivity {
	public static final String PREFS_NAME = "LoginPrefs";
	Button btn_cadastrar;
	Button btn_logar;
	EditText et_usuario, et_senha;

	SessionResponse sessionResponse;
	@RestService
	APIEmergencyWSClient apiEmergencyWSClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homelogin);

		btn_logar = (Button) findViewById(R.id.btn_Entrar);
		btn_cadastrar = (Button) findViewById(R.id.btn_Cadastrar);
		et_senha = (EditText) findViewById(R.id.et_senha);
		et_usuario = (EditText) findViewById(R.id.et_usuario);

		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		if (settings.getString("logged", "").toString().equals("logged")) {
			Intent intent = new Intent(HomeLogin.this, Home.class);
			startActivity(intent);

		}

		btn_logar.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				/*
				 * Intent intent = new Intent(view.getContext(),
				 * LoginActivity_.class); startActivity(intent);
				 */

				// Oculta o Teclado
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(et_usuario.getWindowToken(), 0);

				if (!et_usuario.getText().toString().trim().isEmpty()) {
					et_usuario.setError(null);
				}
				if (!et_senha.getText().toString().trim().isEmpty()) {
					et_senha.setError(null);
				}

				if (et_usuario.getText().toString().trim().equals("")
						|| et_senha.getText().toString().trim().equals("")
						|| !Utils.isEmailValidate(et_usuario.getText()
								.toString())) {

					if (et_usuario.getText().toString().trim().equals("")) {
						et_usuario.setError("Campo obrigatório.");
					}
					if (et_senha.getText().toString().trim().equals("")) {
						et_senha.setError("Campo obrigatório.");
					}
					if (!Utils.isEmailValidate(et_usuario.getText().toString())) {
						et_usuario.setError("Endereço de email inválido.");
					}

				} else {

					ProgressDialog progressDialog;
					progressDialog = ProgressDialog.show(HomeLogin.this, null,
							"Please Wait...", false, true);
					progressDialog.setCancelable(false);

					if (isOnline()) {

						logarUser(progressDialog, et_usuario, et_senha);

						// et_EmailLogin.setText("");
						// et_PassLogin.setText("");
					} else {
						progressDialog.dismiss();
						showToast("Sem conexão com a internet.");
					}
				}

			}
		});

		btn_cadastrar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(HomeLogin.this,
						CadastrarActivity_.class);
				startActivity(intent);

			}
		});
	}

	@Override
	public void onBackPressed() {
		Intent setIntent = new Intent(Intent.ACTION_MAIN);
		setIntent.addCategory(Intent.CATEGORY_HOME);
		setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(setIntent);
	}

	@Background
	public void logarUser(ProgressDialog progressDialog, EditText et_usuario,
			EditText et_senha) {

		try {

			Login login = new Login();
			login.setUsername(et_usuario.getText().toString().trim());
			login.setClientId("be81d991e9e44e5b44b2945df3cf54a1626ad37b75bac81b2ffb22f705960fe3");
			login.setPassword(et_senha.getText().toString().trim());
			login.setGrantType("password");

			sessionResponse = apiEmergencyWSClient.login(login);

			progressDialog.dismiss();
			Intent intent = new Intent(HomeLogin.this, Home.class);
			startActivity(intent);

			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("logged", "logged");
			editor.putString("token", sessionResponse.getAccess_token());
			editor.commit();

			showToast("Usuário logado com sucesso.");

		} catch (Exception e) {
			progressDialog.dismiss();
			e.printStackTrace();
			System.out.println(e);
			showToast("Usuário inválido.");
		}

	}

	@UiThread
	protected void showToast(Object object) {
		et_usuario.setText("");
		et_senha.setText("");
		Toast.makeText(getApplicationContext(), object.toString(),
				Toast.LENGTH_LONG).show();

	}

	// Verifica conexão com a internet.
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

}