package com.lme.emergency190;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.widget.Toast;

public class RecordAudio {
	
	private File outputFile = null;
	private MediaRecorder myRecorder;
	private MediaPlayer myPlayer;
	private Context context;

	
	public RecordAudio(Context context){
		this.context = context;
	}
	
	public void stopRecording() {
		if(myRecorder != null){
			myRecorder.stop();
			myRecorder.release();
			myRecorder = null;
			
			Toast.makeText(context, "Stop recording...", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void play(){
		myPlayer = new MediaPlayer();
		try {
			if(outputFile != null){
				myPlayer.setDataSource(outputFile.getPath());
				myPlayer.prepare();
				myPlayer.start();
				
				Toast.makeText(context, "Playing recording...", Toast.LENGTH_SHORT).show();
			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void recordAudio(){
		
		outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "myaudiorecord.mp4");
		
		if(!outputFile.exists()){
			try {
				outputFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		myRecorder = new MediaRecorder();
		myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		myRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		myRecorder.setAudioEncoder(MediaRecorder.OutputFormat.MPEG_4);
		myRecorder.setOutputFile(outputFile.getPath());
		
		try {
			myRecorder.prepare();
			myRecorder.start();
			
			Toast.makeText(context, "Start recording...", Toast.LENGTH_SHORT).show();

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public File obterAudioRecorded(){
		
		if(outputFile != null && outputFile.exists()){
			return outputFile;
		}
		return null;
	}
}
