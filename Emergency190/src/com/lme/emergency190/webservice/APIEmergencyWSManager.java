package com.lme.emergency190.webservice;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import android.app.Activity;
import android.widget.Toast;

import com.googlecode.androidannotations.annotations.AfterInject;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EBean;
import com.googlecode.androidannotations.annotations.RootContext;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.res.StringRes;
import com.googlecode.androidannotations.annotations.rest.RestService;
import com.lme.emergency190.model.SessionResponse;
import com.lme.emergency190.model.User;
import com.lme.emergency190.model.UserMessage;

@EBean
public class APIEmergencyWSManager {

	@RestService
	APIEmergencyWSClient apiEmergencyWSClient;
	
	@RootContext
	Activity rootActivity;
	
	@StringRes
	public String msg_configurado;
	
	SessionResponse sessionResponse;
	
	public APIEmergencyWSClient getApiEmergencyWSClient(){
		return apiEmergencyWSClient;
	}
	
	@AfterInject
	public void config() {
		setTimeout();
	}
	
	private void setTimeout() {
		int timeoutSeconds = 5;
		ClientHttpRequestFactory requestFactory = apiEmergencyWSClient.getRestTemplate().getRequestFactory();
		if (requestFactory instanceof SimpleClientHttpRequestFactory) {
			// Log.d("HTTP", "HttpUrlConnection is used");
			((SimpleClientHttpRequestFactory) requestFactory).setConnectTimeout(timeoutSeconds * 1000);
			((SimpleClientHttpRequestFactory) requestFactory).setReadTimeout(timeoutSeconds * 1000);
		} else if (requestFactory instanceof HttpComponentsClientHttpRequestFactory) {
			// Log.d("HTTP", "HttpClient is used");
			((HttpComponentsClientHttpRequestFactory) requestFactory).setReadTimeout(timeoutSeconds * 1000);
			((HttpComponentsClientHttpRequestFactory) requestFactory).setConnectTimeout(timeoutSeconds * 1000);
		}
	}
	
	@Background
	protected void wsTestUser() {
		try {
			
			User user = new User();
			user.setEmail("test@gmail.com");
			user.setFirstName("first name");
			user.setLastName("last name");
			user.setPassword("123456");
			
			UserMessage userMessage = new UserMessage();
			
			userMessage.setUser(user);
			
			sessionResponse = apiEmergencyWSClient.insertUser(userMessage);
			
			if (sessionResponse != null && !sessionResponse.getAccess_token().isEmpty()) {
				showToast("Cadastro Realizado com sucesso");
			}

		} catch (Exception e) {
			showToast("Erro ao cadastrar");
			e.printStackTrace();
		}
	}
	
	@UiThread
	void showToast(Object a) {
		Toast.makeText(rootActivity, a.toString(), Toast.LENGTH_LONG).show();
	}
}
