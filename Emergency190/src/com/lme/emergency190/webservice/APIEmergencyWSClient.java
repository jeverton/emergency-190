package com.lme.emergency190.webservice;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.googlecode.androidannotations.annotations.rest.Post;
import com.googlecode.androidannotations.annotations.rest.Rest;
import com.lme.emergency190.model.Login;
import com.lme.emergency190.model.OccurrenceMessage;
import com.lme.emergency190.model.OccurrenceResponse;
import com.lme.emergency190.model.SessionResponse;
import com.lme.emergency190.model.UserMessage;

@Rest(rootUrl = "http://200.129.43.154:82", converters = {GsonHttpMessageConverter.class})
public interface APIEmergencyWSClient {

	@Post("/users")
	SessionResponse insertUser(UserMessage user);
	
	@Post("/oauth/token")
	SessionResponse login(Login login);
	
	@Post("/occurrences?access_token={access_token}")
	OccurrenceResponse insertOccurence(OccurrenceMessage occurrence, String access_token);
	
	RestTemplate getRestTemplate();
	
	
}
