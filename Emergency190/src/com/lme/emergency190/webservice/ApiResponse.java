package com.lme.emergency190.webservice;

import org.springframework.http.HttpStatus;

public class ApiResponse <T extends Object> {

	private HttpStatus statusCode;
	private Object result;
	
	public HttpStatus getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	
}
