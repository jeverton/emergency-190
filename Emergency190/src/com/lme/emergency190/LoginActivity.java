package com.lme.emergency190;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.rest.RestService;
import com.lme.emergency190.model.Login;
import com.lme.emergency190.model.SessionResponse;
import com.lme.emergency190.webservice.APIEmergencyWSClient;

@SuppressLint("NewApi")
@EActivity
public class LoginActivity extends SherlockActivity {
	public static final String PREFS_NAME = "LoginPrefs";

	Button btn_Login;
	EditText et_EmailLogin, et_PassLogin;

	SessionResponse sessionResponse;
	@RestService
	APIEmergencyWSClient apiEmergencyWSClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		Button btn_Login = (Button) findViewById(R.id.btn_Login);

		btn_Login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				et_EmailLogin = (EditText) findViewById(R.id.et_EmailLogin);
				et_PassLogin = (EditText) findViewById(R.id.et_PassLogin);

				// Oculta o Teclado
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(et_EmailLogin.getWindowToken(), 0);

				if (!et_EmailLogin.getText().toString().trim().isEmpty()) {
					et_EmailLogin.setError(null);
				}
				if (!et_PassLogin.getText().toString().trim().isEmpty()) {
					et_PassLogin.setError(null);
				}

				if (et_EmailLogin.getText().toString().trim().equals("")
						|| et_PassLogin.getText().toString().trim().equals("")
						|| !Utils.isEmailValidate(et_EmailLogin.getText()
								.toString())) {

					if (et_EmailLogin.getText().toString().trim().equals("")) {
						et_EmailLogin.setError("Campo obrigatório.");
					}
					if (et_PassLogin.getText().toString().trim().equals("")) {
						et_PassLogin.setError("Campo obrigatório.");
					}
					if (!Utils.isEmailValidate(et_EmailLogin.getText()
							.toString())) {
						et_EmailLogin.setError("Endereço de email inválido.");
					}

				} else {

					ProgressDialog progressDialog;
					progressDialog = ProgressDialog.show(LoginActivity.this,
							null, "Please Wait...", false, true);
					progressDialog.setCancelable(false);

					if (isOnline()) {

						logarUser(progressDialog, et_EmailLogin, et_PassLogin);

						// et_EmailLogin.setText("");
						// et_PassLogin.setText("");
					} else {
						progressDialog.dismiss();
						showToast("Sem conexão com a internet.");
					}
				}

			}
		});
	}

	@Background
	public void logarUser(ProgressDialog progressDialog, EditText et_login,
			EditText et_password) {

		try {

			Login login = new Login();
			login.setUsername(et_login.getText().toString().trim());
			login.setClientId("be81d991e9e44e5b44b2945df3cf54a1626ad37b75bac81b2ffb22f705960fe3");
			login.setPassword(et_password.getText().toString().trim());
			login.setGrantType("password");

			sessionResponse = apiEmergencyWSClient.login(login);

			progressDialog.dismiss();
			Intent intent = new Intent(LoginActivity.this, Home.class);
			startActivity(intent);

			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("logged", "logged");
			editor.putString("token", sessionResponse.getAccess_token());
			editor.commit();

			showToast("Usuário logado com sucesso.");

		} catch (Exception e) {
			progressDialog.dismiss();
			showToast("Usuário inválido.");
		}

	}

	@UiThread
	protected void showToast(Object object) {
		et_EmailLogin.setText("");
		et_PassLogin.setText("");
		Toast.makeText(getApplicationContext(), object.toString(),
				Toast.LENGTH_LONG).show();
		
	}

	@Override
	public void onBackPressed() {
		Intent i = new Intent(LoginActivity.this, HomeLogin.class);
		startActivity(i);
	}

	// Verifica conexão com a internet.
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

}