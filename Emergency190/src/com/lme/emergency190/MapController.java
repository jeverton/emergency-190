package com.lme.emergency190;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;

public class MapController implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {
	private Context mContext;
	private Activity mActivity;
	private FragmentManager mFragmentManager;
	private GoogleMap mMap;
	private UiSettings uiSettings;
	private LocationClient mLocationClient;
	private OnConnected mOnConnected;
	// private boolean isOpenning;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	public MapController(Context context, Activity activity,
			FragmentManager fragmentManager, OnConnected onConnected,
			GoogleMap map) {
		mContext = context;
		mActivity = activity;
		mFragmentManager = fragmentManager;
		mOnConnected = onConnected;
		mMap = map;
		uiSettings = mMap.getUiSettings();
	}

	public void setMapConfig() {
		mLocationClient = new LocationClient(mContext, this, this);
		mMap.setMyLocationEnabled(false);
		uiSettings.setZoomControlsEnabled(true);
		/*
		 * uiSettings.setCompassEnabled(false);
		 * uiSettings.setZoomControlsEnabled(false);
		 * uiSettings.setMyLocationButtonEnabled(false);
		 * uiSettings.setAllGesturesEnabled(false);
		 */
	}

	public boolean isGooglePlayServicesAvailable() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(mContext);
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d("Location Updates", "Google Play services is available.");
			return true;
		} else {
			// Get the error dialog from Google Play services
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
					resultCode, mActivity,
					CONNECTION_FAILURE_RESOLUTION_REQUEST);

			// If Google Play services can provide an error dialog
			if (errorDialog != null) {
				// Create a new DialogFragment for the error dialog
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(errorDialog);
				errorFragment.show(mFragmentManager, "Location Updates");
			}

			return false;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// TODO Auto-generated method stub
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(mActivity,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);

				/*
				 * * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */

			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
			Toast.makeText(mContext,
					"Sorry. Location services not available to you",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		// TODO Auto-generated method stub
		try {
			Location location = mLocationClient.getLastLocation();
			LatLng latLng = new LatLng(location.getLatitude(),
					location.getLongitude());
			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
			mOnConnected.onConnected(latLng, true);

		} catch (NullPointerException e) {
			Toast.makeText(mContext,
					"Não foi possível resgatar localização atual.",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	public interface OnConnected {
		public void onConnected(LatLng latLng, boolean isOpenning);
	}

	public LocationClient getmLocationClient() {
		return mLocationClient;
	}

	public UiSettings getUiSettings() {
		return uiSettings;
	}

}

class ErrorDialogFragment extends DialogFragment {

	// Global field to contain the error dialog
	private Dialog mDialog;

	// Default constructor. Sets the dialog field to null
	public ErrorDialogFragment() {
		super();
		mDialog = null;
	}

	// Set the dialog to display
	public void setDialog(Dialog dialog) {
		mDialog = dialog;
	}

	// Return a Dialog to the DialogFragment.
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return mDialog;
	}
}
