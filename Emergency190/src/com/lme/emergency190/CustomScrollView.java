package com.lme.emergency190;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

public class CustomScrollView extends ScrollView{

	private GestureDetector gestureDetector;
	View.OnTouchListener gestureListener;
	private float mDownX;
	private float mDownY;
	private final float SCROLL_THRESHOLD = 10;
	private boolean isOnClick;
	
	@Deprecated
	public CustomScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		gestureDetector = new GestureDetector(new YScrollDetector());
		setFadingEdgeLength(0);
	}
	
	// android.support.v4.view.ViewPager
		@Override
		public boolean onTouchEvent(MotionEvent ev) {

			return super.onTouchEvent(ev);
		}

		@Override
		public boolean onInterceptTouchEvent(MotionEvent ev) {
			boolean result = super.onInterceptTouchEvent(ev);
			if (gestureDetector.onTouchEvent(ev)) {
				return result;
			}
			else {
				this.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		}
		
		@Override
		public boolean dispatchTouchEvent(MotionEvent ev) {
			switch (ev.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				mDownX = ev.getX();
				mDownY = ev.getY();
				isOnClick = true;
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				if (isOnClick) {
					//map_fragment.bringToFront();
				}
				break;
			case MotionEvent.ACTION_MOVE:
				if (isOnClick
						&& (Math.abs(mDownX - ev.getX()) > SCROLL_THRESHOLD || Math
								.abs(mDownY - ev.getY()) > SCROLL_THRESHOLD)) {
					//findViewById(R.id.home_scrollView).bringToFront();
					//findViewById(R.id.relative_actionbar).invalidate();
					
					System.out.println("MOVIMENTOU O MAPAAAAA");
					isOnClick = false;
				}
				break;
			default:
				break;
			}
			return super.dispatchTouchEvent(ev);
		}

		// Return false if we're scrolling in the x direction
		class YScrollDetector extends SimpleOnGestureListener {
			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2,
					float distanceX, float distanceY) {
				try {
					if (Math.abs(distanceY) > Math.abs(distanceX)) {
						return false;
					} else {
						return true;
					}
				} catch (Exception e) {
					// nothing
				}
				return false;
			}
		}

}