package com.lme.emergency190;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class Home extends SherlockActivity implements OnClickListener,
		OnMenuItemClickListener{
	public static final String PREFS_NAME = "LoginPrefs";

	Button button_type1, button_type2, button_type3, button_type4,
			button_type5, button_type6, home_button_service1,
			home_button_service2, home_button_service3;
	ActionBar bar;
	private SlidingMenu menu;
	private ArrayList<MenuItem> menuArray, lastItemMenu;
	private ListView list, last_item_menu;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		/*
		 * Drawable bgBar = getResources().getDrawable(R.drawable.bg);
		 * CharSequence myString = "Segurança Pública"; SpannableString
		 * spannable = new SpannableString(myString); spannable.setSpan(new
		 * ForegroundColorSpan(Color.WHITE), 0, myString.length(), 0);
		 */

		// bar = getSupportActionBar();
		// bar.setBackgroundDrawable(null);
		// bar.setTitle(spannable);
		// bar.setBackgroundDrawable(bgBar);
		// bar.setHomeButtonEnabled(true);
		// bar.setDisplayHomeAsUpEnabled(true);
		
		setMenuConfig();

		button_type1 = (Button) findViewById(R.id.home_button_type1);
		button_type1.setOnClickListener(this);

		button_type2 = (Button) findViewById(R.id.home_button_type2);
		button_type2.setOnClickListener(this);

		button_type3 = (Button) findViewById(R.id.home_button_type3);
		button_type3.setOnClickListener(this);

		button_type4 = (Button) findViewById(R.id.home_button_type4);
		button_type4.setOnClickListener(this);

		button_type5 = (Button) findViewById(R.id.home_button_type5);
		button_type5.setOnClickListener(this);

		button_type6 = (Button) findViewById(R.id.home_button_type6);
		button_type6.setOnClickListener(this);

		home_button_service1 = (Button) findViewById(R.id.home_button_service1);
		home_button_service1.setOnClickListener(this);

		home_button_service2 = (Button) findViewById(R.id.home_button_service2);
		home_button_service2.setOnClickListener(this);

		home_button_service3 = (Button) findViewById(R.id.home_button_service3);
		home_button_service3.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
		case R.id.home_button_type1:
			intent = new Intent(v.getContext(), Occurrence_Activity_.class);
			intent.putExtra("type", Constants.TYPE_ROUBO);
			startActivity(intent);
			break;

		case R.id.home_button_type2:
			intent = new Intent(v.getContext(), Occurrence_Activity_.class);
			intent.putExtra("type", Constants.TYPE_FURTO);
			startActivity(intent);
			break;

		case R.id.home_button_type3:
			intent = new Intent(v.getContext(), Occurrence_Activity_.class);
			intent.putExtra("type", Constants.TYPE_HOMICIDIO);
			startActivity(intent);
			break;

		case R.id.home_button_type4:
			intent = new Intent(v.getContext(), Occurrence_Activity_.class);
			intent.putExtra("type", Constants.TYPE_DROGAS);
			startActivity(intent);
			break;

		case R.id.home_button_type5:
			intent = new Intent(v.getContext(), Occurrence_Activity_.class);
			intent.putExtra("type", Constants.TYPE_SUSPEITO);
			startActivity(intent);
			break;

		case R.id.home_button_type6:
			intent = new Intent(v.getContext(), Occurrence_Activity_.class);
			intent.putExtra("type", Constants.OTHER_TYPE);
			startActivity(intent);
			break;

		case R.id.home_button_service1:
			intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel: 190"));
			startActivity(intent);
			break;

		case R.id.home_button_service2:
			intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel: 192"));
			startActivity(intent);
			break;

		case R.id.home_button_service3:
			intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel: 193"));

			startActivity(intent);
			break;
		}

	}


	@Override
	public boolean onMenuItemClick(android.view.MenuItem item) {
		if (item.getItemId() == 0) {

			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.remove("logged");
			editor.remove("token");
			editor.commit();
			finish();
			Toast.makeText(getApplicationContext(),
					"Logout realizado com sucesso.", Toast.LENGTH_LONG).show();
		}
		return false;
	}
	
	//Sliding Menu
		private void setMenuConfig() {
			menu = new SlidingMenu(this);
			menuArray = new ArrayList<MenuItem>();
			lastItemMenu = new ArrayList<MenuItem>();
			
			MenuItem menuItem1 = new MenuItem();
			menuItem1.setImageMenuItem(getResources().getDrawable(R.drawable.inf_menu));
			menuItem1.setTextMenuItem("Informar Problema");
			
			MenuItem menuItem2 = new MenuItem();
			menuItem2.setImageMenuItem(getResources().getDrawable(R.drawable.sob_menu));
			menuItem2.setTextMenuItem("Sobre");
			
			MenuItem lastItem = new MenuItem();
			lastItem.setImageMenuItem(getResources().getDrawable(R.drawable.out_menu));
			lastItem.setTextMenuItem("Sair");
			lastItem.setIsLastItem();
			
			menuArray.add(menuItem1);
			menuArray.add(menuItem2);
			lastItemMenu.add(lastItem);

			menu.setMode(SlidingMenu.LEFT);
			menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			menu.setShadowWidthRes(R.dimen.shadow_width);
			menu.setShadowDrawable(R.drawable.shadow_menu);
			menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
			menu.setFadeDegree(0.25f);
			menu.setBehindScrollScale(0.25f);
			menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
			menu.setSelectorDrawable(R.drawable.ic_launcher);
			menu.setMenu(R.layout.menu);

			list = (ListView) findViewById(R.id.menu_list);
			list.setAdapter(new ListViewAdapter(Home.this,
					R.layout.drawer_list_item, menuArray));
			
			last_item_menu = (ListView) findViewById(R.id.last_menu_item);
			last_item_menu.setAdapter(new ListViewAdapter(Home.this,
					R.layout.drawer_list_item, lastItemMenu));
		}
		
		// List View Menu
		class ListViewAdapter extends ArrayAdapter<MenuItem> {

			private Context context;
			private int layoutResourceId;
			private ArrayList<MenuItem> entries;

			public ListViewAdapter(Context context, int textViewResourceId,
					ArrayList<MenuItem> entries) {
				super(context, textViewResourceId, entries);
				this.context = context;
				this.layoutResourceId = textViewResourceId;
				this.entries = entries;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = convertView;
				TextView text;
				ImageView image;
				LinearLayout top_row;
				LinearLayout bottom_row;

				if (v == null) {
					LayoutInflater inflater = (LayoutInflater) context
							.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = inflater.inflate(layoutResourceId, parent, false);

					image = (ImageView) v.findViewById(R.id.menu_item_icon);
					image.setImageDrawable(entries.get(position).getImageMenuItem());
					
					text = (TextView) v.findViewById(R.id.menu_item_text);
					text.setText(entries.get(position).getTextMenuItem());
					
					top_row = (LinearLayout) v.findViewById(R.id.top_row);
					bottom_row = (LinearLayout) v.findViewById(R.id.bottom_row);
					
					if(entries.get(position).getIsLastItem()){
						top_row.setVisibility(View.VISIBLE);
						bottom_row.setVisibility(View.GONE);
					} else {
						top_row.setVisibility(View.GONE);
						bottom_row.setVisibility(View.VISIBLE);
					}
					
				}

				v.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						
					}
				});
				return v;
			}
		}
		
		@Override
		public void onBackPressed() {
			Intent setIntent = new Intent(Intent.ACTION_MAIN);
			setIntent.addCategory(Intent.CATEGORY_HOME);
			setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(setIntent);
			/* MENU LATERAL APARECER
			 if (menu.isMenuShowing()) {
				menu.toggle();
				return;
			} else {
				super.onBackPressed();
			}
			 */
		}
		
		class MenuItem {
			
			private Drawable image_menu_item;
			private String text_menu_item;
			private Boolean isLastItem = false;
			
			public Drawable getImageMenuItem(){
				return image_menu_item;
			}
			
			public void setImageMenuItem(Drawable drawable){
				this.image_menu_item = drawable;
			}
			
			public String getTextMenuItem(){
				return text_menu_item;
			}
			
			public void setTextMenuItem(String text){
				this.text_menu_item = text;
			}
			
			public Boolean getIsLastItem(){
				return isLastItem;
			}
			
			public void setIsLastItem(){
				this.isLastItem = true;
			}
			
		}
		
		

	// public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu)
	// {
	// menu.add(0, 0, 1, "Logout").setShowAsAction(
	// MenuItem.SHOW_AS_ACTION_NEVER
	// | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
	//
	// return super.onCreateOptionsMenu(menu);
	// }

	// public boolean onOptionsItemSelected(MenuItem item) {
	// if (item.getItemId() == 0) {
	// SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
	// SharedPreferences.Editor editor = settings.edit();
	// editor.remove("logged");
	// editor.commit();
	// finish();
	// Toast.makeText(getApplicationContext(),
	// "Logout realizado com sucesso.", Toast.LENGTH_LONG).show();
	// }
	// return false;
	// }

}