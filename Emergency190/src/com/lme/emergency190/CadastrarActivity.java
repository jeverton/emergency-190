package com.lme.emergency190;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.rest.RestService;
import com.lme.emergency190.model.SessionResponse;
import com.lme.emergency190.model.User;
import com.lme.emergency190.model.UserMessage;
import com.lme.emergency190.webservice.APIEmergencyWSClient;

@SuppressLint("NewApi")
@EActivity
public class CadastrarActivity extends SherlockActivity {

	SessionResponse sessionResponse;
	@RestService
	APIEmergencyWSClient apiEmergencyWSClient;

	EditText et_Email, et_FirstName, et_LastName, et_Password,
			et_ConfirmPassword;
	Button btn_confirmar_cadastro, btn_reset;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastrar);

		et_Email = (EditText) findViewById(R.id.et_Email);
		et_FirstName = (EditText) findViewById(R.id.et_FirstName);
		et_LastName = (EditText) findViewById(R.id.et_LastName);
		et_Password = (EditText) findViewById(R.id.et_Password);
		et_ConfirmPassword = (EditText) findViewById(R.id.et_ConfirmPassword);
		btn_confirmar_cadastro = (Button) findViewById(R.id.btn_confirmar_cadastro);
		btn_reset = (Button) findViewById(R.id.btn_reset);

		btn_confirmar_cadastro.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Ocultar o teclado
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(et_Password.getWindowToken(), 0);

				if (!et_FirstName.getText().toString().trim().isEmpty()) {
					et_FirstName.setError(null);
				}
				if (!et_LastName.getText().toString().trim().isEmpty()) {
					et_LastName.setError(null);
				}
				if (!et_Password.getText().toString().trim().isEmpty()) {
					et_Password.setError(null);
				}
				if (!et_ConfirmPassword.getText().toString().trim().isEmpty()) {
					et_ConfirmPassword.setError(null);
				}
				if (!et_Email.getText().toString().trim().isEmpty()) {
					et_Email.setError(null);
				}

				if (et_FirstName.getText().toString().trim().equals("")
						|| et_Email.getText().toString().trim().equals("")
						|| et_LastName.getText().toString().trim().equals("")
						|| et_Password.getText().toString().trim().equals("")
						|| et_ConfirmPassword.getText().toString().trim()
								.equals("")
						|| !Utils
								.isEmailValidate(et_Email.getText().toString())
						|| !et_Password
								.getText()
								.toString()
								.equals(et_ConfirmPassword.getText().toString())
						|| et_Password.getText().toString().contains(" ")
						|| et_ConfirmPassword.getText().toString()
								.contains(" ")
						|| et_ConfirmPassword.length() < 6
						|| et_Password.length() < 6) {

					if (et_FirstName.getText().toString().trim().equals("")) {
						et_FirstName.setError("Campo obrigatório.");

					}
					if (et_LastName.getText().toString().trim().equals("")) {
						et_LastName.setError("Campo obrigatório.");

					}
					if (et_Password.getText().toString().trim().equals("")) {
						et_Password.setError("Campo obrigatório.");
					}
					if (et_ConfirmPassword.getText().toString().trim()
							.equals("")) {
						et_ConfirmPassword.setError("Campo obrigatório.");
					}
					if (et_Email.getText().toString().trim().equals("")) {
						et_Email.setError("Campo obrigatório.");
					}
					if (et_Password.getText().toString().trim().length() < 6) {
						et_Password
								.setError("A senha precisa ter pelo menos 6 dígitos.");
						et_Password.setText("");
					}
					if (et_ConfirmPassword.getText().toString().trim().length() < 6) {
						et_ConfirmPassword
								.setError("A senha precisa ter pelo menos 6 dígitos.");
						et_ConfirmPassword.setText("");
					}

					if (!et_Password.getText().toString()
							.equals(et_ConfirmPassword.getText().toString())) {
						et_Password.setText("");
						et_ConfirmPassword.setText("");
						et_Password.setError("Senha inválida.");
						et_ConfirmPassword.setError("Senha inválida.");
					}

					if (!Utils.isEmailValidate(et_Email.getText().toString())) {
						et_Email.setText("");

						et_Email.setError("Endereço de email inválido.");
					}
					if (et_Password.getText().toString().contains(" ")) {
						et_Password.setError("Caractere inválido.");
						et_Password.setText("");
					}
					if (et_ConfirmPassword.getText().toString().contains(" ")) {
						et_ConfirmPassword.setError("Caractere inválido.");
						et_ConfirmPassword.setText("");
					}

				} else {

					ProgressDialog progressDialog;
					progressDialog = ProgressDialog.show(
							CadastrarActivity.this, null, "Please Wait...",
							false, true);

					if (isOnline()) {
						cadastrarUser(progressDialog);
						Intent intent = new Intent(getApplicationContext(),
								HomeLogin_.class);
						startActivity(intent);

					} else {
						progressDialog.dismiss();
						showToast("Sem conexão com a internet");
					}
				}

			}
		});

		btn_reset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				et_Email.setText("");
				et_Password.setText("");
				et_LastName.setText("");
				et_FirstName.setText("");
				et_ConfirmPassword.setText("");

			}

		});
	}

	@Background
	public void cadastrarUser(ProgressDialog progressDialog) {

		try {

			User user = new User();
			user.setEmail(et_Email.getText().toString().trim());
			user.setFirstName(et_FirstName.getText().toString().trim());
			user.setLastName(et_LastName.getText().toString().trim());
			user.setPassword(et_Password.getText().toString().trim());

			UserMessage userMessage = new UserMessage();

			userMessage.setUser(user);

			sessionResponse = apiEmergencyWSClient.insertUser(userMessage);

			progressDialog.dismiss();

			showToast("Cadastro realizado com sucesso.");

		} catch (Exception e) {
			showToast("Cadastro inválido, tente novamente.");
		}

	}

	@UiThread
	protected void showToast(Object object) {
		Toast.makeText(getApplicationContext(), object.toString(),
				Toast.LENGTH_LONG).show();
	}

	// Verifica conexão com a internet.
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

}