package com.lme.emergency190;

public class Constants {

	//Constants
	public static final Integer TYPE_ROUBO = 1;
	public static final Integer TYPE_FURTO = 2;
	public static final Integer TYPE_HOMICIDIO = 3;
	public static final Integer TYPE_DROGAS = 4;
	public static final Integer TYPE_SUSPEITO = 5;
	public static final Integer OTHER_TYPE = 6;
	
}
