package com.lme.emergency190;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.web.client.HttpClientErrorException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.rest.RestService;
import com.lme.emergency190.MapController.OnConnected;
import com.lme.emergency190.model.Assets;
import com.lme.emergency190.model.Occurrence;
import com.lme.emergency190.model.OccurrenceMessage;
import com.lme.emergency190.model.OccurrenceResponse;
import com.lme.emergency190.webservice.APIEmergencyWSClient;
@EActivity
@SuppressLint("NewApi")
public class Occurrence_Activity extends SherlockFragmentActivity implements
		OnConnected, OnClickListener, OnMenuItemClickListener {

	public static final String PREFS_NAME = "LoginPrefs";
	public static final int REQUEST_IMAGE_CAPTURE = 1;
	public static final int REQUEST_VIDEO = 2;
	public static final int REQUEST_RECORD_AUDIO = 3;
	public static final int REQUEST_LOAD_IMAGE = 4;
	public static final int REQUEST_LOAD_AUDIO = 5;

	private MapController mapController;
	GoogleMap map;
	MarkerOptions markerOptions;
	LatLng latLng;
	EditText occurrence_local_input;
	EditText occurrence_obs_input;
	String addressText = "";
	Button search_address;
	ActionBar bar;
	Menu menu;
	PopupMenu popupMenu;
	Button img_button_top_ab, btnImagem, btnAudio, btnVideo, btnSend;
	Location mLocation;
	LatLng mLatLng;
	EditText occurrence_date, occurrence_hour;
	String stringHour;
	TextView text_bar;
	View map_fragment;
	
	@RestService
	APIEmergencyWSClient apiEmergencyWSClient;
	private Uri photoLocalURI;
	private File fileImage;
	private List<File> listaArquivosUsuario = new ArrayList<File>();
	private ImageView imageViewImage;
	private Occurrence occurrence = new Occurrence();
	private int oType;
	private String mimeType, token;
	private Assets assets = new Assets();
	private OccurrenceMessage occurrenceMessage = new OccurrenceMessage();
	private RecordAudio recordAudio;
	private TextView textViewAudio;
	private Uri videoLocalURI = null;

	private int mYear;
	private int mMonth;
	private int mDay;

	private int mhour;
	private int mminute;

	static final int TIME_DIALOG_ID = 1;
	static final int DATE_DIALOG_ID = 0;

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		if (mapController.isGooglePlayServicesAvailable()) {
			mapController.getmLocationClient().connect();
		}
		super.onStart();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		mapController.getmLocationClient().disconnect();
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_occurrence);

		text_bar = (TextView) findViewById(R.id.tv_actionbar);
		text_bar.setOnClickListener(this);
		
		btnImagem = (Button) findViewById(R.id.occurrence_button_attach_type1);
		btnAudio = (Button) findViewById(R.id.occurrence_button_attach_type2);
		btnVideo = (Button) findViewById(R.id.occurrence_button_attach_type3);
		btnSend = (Button) findViewById(R.id.send_button);
		
		imageViewImage = (ImageView) findViewById(R.id.imageview);
		textViewAudio = (TextView) findViewById(R.id.tv_audio);

		occurrence_date = (EditText) findViewById(R.id.occurence_date);
		occurrence_hour = (EditText) findViewById(R.id.occurence_datetime);
		
		recordAudio = new RecordAudio(getApplicationContext());
		
		map_fragment = findViewById(R.id.map_occurrence);
		
		btnClicks();		
		oType = obterTipo();
		token = obterToken();

		Time now2 = new Time();
		now2.setToNow();

		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();
		String month2 = null;
		int month = now2.month + 1;
		if (month < 10) {
			month2 = "0" + month;
		}
		if (now2.hour < 10) {
			stringHour = "0" + now2.hour;
		} else {
			stringHour = "" + now2.hour;
		}
		
		now2.minute = (Integer) (now2.minute < 10 ? Integer.parseInt("0" + now2.minute) : now2.minute);
		occurrence_date.setHint(now2.monthDay + "/" + month2 + "/" + now2.year);
		occurrence_hour.setHint(stringHour + ":" + now2.minute);

		//Pick time's click event listener
		occurrence_date.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(DATE_DIALOG_ID);
			}

		});

		//PickDate's click event listener
		occurrence_hour.setOnClickListener(new View.OnClickListener() {
			@SuppressWarnings("deprecation")
			public void onClick(View v) {
				showDialog(TIME_DIALOG_ID);

			}
		});

		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mhour = c.get(Calendar.HOUR_OF_DAY);
		mminute = c.get(Calendar.MINUTE);

		occurrence_local_input = (EditText) findViewById(R.id.occurence_local_input);
		occurrence_local_input
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							if (occurrence_local_input.getText().toString() != "") {
								new GeocodingTaskSearchPressed(getBaseContext())
										.execute(occurrence_local_input
												.getText().toString());
							}
							return true;
						}
						return false;
					}
				});
		occurrence_local_input.setOnClickListener(this);
		occurrence_obs_input = (EditText) findViewById(R.id.occurence_obs_input);

		//setting map
		SupportMapFragment smf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_occurrence);
		map = smf.getMap();

		Activity activity = (Activity) this;
		mapController = new MapController(this, activity,
				getSupportFragmentManager(), this, map);

		mapController.setMapConfig();
		//mapController.getUiSettings().setMyLocationButtonEnabled(true);

		map.setOnMapClickListener(new com.google.android.gms.maps.GoogleMap.OnMapClickListener() {

			@Override
			public void onMapClick(LatLng arg0) {

				// Getting the Latitude and Longitude of the touched location
				latLng = arg0;

				// Clears the previously touched position
				map.clear();

				// Animating to the touched position
				map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

				// Creating a marker
				markerOptions = new MarkerOptions();

				// Setting the position for the marker
				markerOptions.position(latLng);

				// Placing a marker on the touched position
				map.addMarker(markerOptions);

				// Adding Marker on the touched location with address
				new GeocodingTaskMapTouched(getBaseContext()).execute(latLng);

			}
		});
		
		

		occurrence_obs_input
				.setOnFocusChangeListener(new OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
							occurrence_obs_input.setText("");
						} else {
						}
					}
				});
		
		occurrence_obs_input.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				occurrence.setObs(occurrence_obs_input.getText().toString());
			}
		});

		occurrence_local_input.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (occurrence_local_input.getText().toString()
						.equals(addressText)) {
					occurrence_local_input.setTextColor(Color
							.parseColor("#999999"));
				} else {
					occurrence_local_input.setTextColor(Color.RED);
				}
			}
		});
	}

	private class GeocodingTaskMapTouched extends
			AsyncTask<LatLng, Void, String> {
		Context mContext;

		public GeocodingTaskMapTouched(Context context) {
			super();
			mContext = context;
		}

		//Finding address using reverse geocoding
		@Override
		protected String doInBackground(LatLng... params) {
			Geocoder geocoder = new Geocoder(mContext);
			double latitude = params[0].latitude;
			double longitude = params[0].longitude;
			List<Address> addresses = null;
			
			occurrence.setLatitude(latitude);
			occurrence.setLongitude(longitude);

			try {
				addresses = geocoder.getFromLocation(latitude, longitude, 1);
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (addresses != null && addresses.size() > 0) {
				Address address = addresses.get(0);

				addressText = address.getMaxAddressLineIndex() > 0 ? address
								.getAddressLine(0) : "";
			}

			return addressText;
		}

		@Override
		protected void onPostExecute(String addressText) {
			occurrence_local_input.setText(addressText);
			occurrence.setAddress(addressText);
		}
	}

	private class GeocodingTaskSearchPressed extends
			AsyncTask<String, Void, String> {
		Context mContext;
		double addressLatitude;
		double addressLongitude;
		Address realAddress;
		String addressName = "";

		public GeocodingTaskSearchPressed(Context context) {
			super();
			mContext = context;
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Geocoder geocoder = new Geocoder(mContext);
			String mAddress = params[0];
			try {
				realAddress = geocoder.getFromLocationName(mAddress, 1).get(0);
				addressName = realAddress.getAddressLine(0);
				addressText = addressName;
				addressLatitude = realAddress.getLatitude();
				addressLongitude = realAddress.getLongitude();
				
				occurrence.setAddress(addressText);
				occurrence.setLatitude(addressLatitude);
				occurrence.setLongitude(addressLongitude);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			// Set Latitude and Logitude
			LatLng addressLatLng = new LatLng(addressLatitude, addressLongitude);

			// Clears the previously touched position
			map.clear();

			// Animating to the touched position
			map.animateCamera(CameraUpdateFactory.newLatLng(addressLatLng));

			// Creating a marker
			markerOptions = new MarkerOptions();

			// Setting the position for the marker
			markerOptions.position(addressLatLng);

			// Placing a marker on the touched position
			map.addMarker(markerOptions);

			// Set EditText
			if (addressName != "") {
				occurrence_local_input.setText(addressName);
			}

		}
	}

	@Override
	public void onConnected(LatLng latLng, boolean isOpenning) {
		mLocation = mapController.getmLocationClient().getLastLocation();
		mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());

		// Clears the previously touched position
		map.clear();
		
		// Creating a marker
		markerOptions = new MarkerOptions();

		// Setting the position for the marker
		markerOptions.position(mLatLng);

		// Placing a marker on the touched position
		map.addMarker(markerOptions);

		// Adding Marker on the current location with address
		new GeocodingTaskMapTouched(getBaseContext()).execute(mLatLng);
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			super.onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void updateDate() {

		String mMonth2 = null;
		String mYear2 = null;
		String mDay2 = null;
		mMonth = mMonth + 1;
		mYear = mYear - 2000;
		// Month is 0 based so add 1
		if (mMonth < 10) {

			mMonth2 = "0" + mMonth;
		} else {
			mMonth2 = "" + mMonth;
		}
		if (mDay < 10) {

			mDay2 = "0" + mDay;
		} else {
			mDay2 = "" + mDay;
		}
		if (mYear < 10) {

			mYear2 = "0" + mYear;
		} else {
			mYear2 = "" + mYear;
		}
		occurrence_date.setHint(new StringBuilder().append(mDay2).append("/")
				.append(mMonth2).append("/").append(mYear2));

	}

	public void updatetime() {
		occurrence_hour.setHint(new StringBuilder().append(Utils.pad(mhour))
				.append(":").append(Utils.pad(mminute)));
	}

	//Datepicker dialog generation
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDate();
		}
	};

	//Timepicker dialog generation
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mhour = hourOfDay;
			mminute = minute;
			updatetime();
		}

	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);

		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, mTimeSetListener, mhour, mminute,
					false);

		}
		return null;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			
			switch (requestCode) {
			case REQUEST_IMAGE_CAPTURE:
				Bitmap bitmap = null;
				try {
					bitmap = MediaStore.Images.Media.getBitmap( getApplicationContext().getContentResolver(),  photoLocalURI);
					fileImage = new File(photoLocalURI.getPath());
					listaArquivosUsuario.add(fileImage);
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if(bitmap != null){
					imageViewImage.setImageBitmap(bitmap);
					imageViewImage.setVisibility(View.VISIBLE);
					textViewAudio.setVisibility(View.GONE);
				}
				
				System.out.println("REQUEST_IMAGE_CAPTURE");
				
				break;
			case REQUEST_LOAD_IMAGE:
				
				Uri selectedImageUri = data.getData();				 
                String tempPath = getPath(selectedImageUri, Occurrence_Activity.this);
                fileImage = new File(tempPath);
                listaArquivosUsuario.add(fileImage);
                Bitmap bm;
                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
                imageViewImage.setImageBitmap(bm);
                imageViewImage.setVisibility(View.VISIBLE);
                textViewAudio.setVisibility(View.GONE);
				
				System.out.println("REQUEST_LOAD_IMAGE");
				
				break;
			case REQUEST_LOAD_AUDIO:
				
				break;
			case REQUEST_RECORD_AUDIO:
				
				System.out.println("REQUEST_RECORD_AUDIO");
				
				break;
			case REQUEST_VIDEO:
				
				if(data != null){
					File video = new File(videoLocalURI.getPath());
					listaArquivosUsuario.add(video);
				}
				System.out.println("REQUEST_VIDEO");
				
				break;

			}
			
		}
	}
	
	private void takePhoto(){
		Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File photoLocal = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		File photo = new File(photoLocal, "imagemTeste.jpg");
		
		if(!photo.exists()){
			try {
				photo.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		photoLocalURI = Uri.fromFile(photo);
		takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoLocalURI);
		
	    if (takePhotoIntent.resolveActivity(getPackageManager()) != null) {
	        startActivityForResult(takePhotoIntent, REQUEST_IMAGE_CAPTURE);
	    }
	}
	
	private void takeVideo(){
		
		Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		File videoLocal = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
		
		File video = new File(videoLocal, "myvideo.mp4");
						
		if(video.exists()){
			video.delete();
		}
		
		if(!video.exists()){
			try {
				video.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		videoLocalURI = Uri.fromFile(video);
		takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoLocalURI);
		takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
		takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 60);
		takeVideoIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, 1024*1024);
		
	    if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
	        startActivityForResult(takeVideoIntent, REQUEST_VIDEO);
	    }
	}
	
	private void createDialogImage(){
		AlertDialog.Builder builder = new AlertDialog.Builder(Occurrence_Activity.this);
		builder.setTitle(R.string.occurrence_dialog_title_imagem);
		builder.setCancelable(false);
		builder.setItems(R.array.choose_images, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				switch (which) {
				case 0:
					takePhoto();
					break;
				case 1:
					takePictureFromGallery();
					break;
				case 2:
					dialog.dismiss();
					break;

				}
			}
		});
		
		builder.show();
	}
	
	private void createDialogAudio(){
		
		final Dialog dialog = new Dialog(Occurrence_Activity.this);
		dialog.setContentView(R.layout.dialog_audio_record);
		dialog.setTitle(R.string.dialog_record_title);
		dialog.setCancelable(false);
		
		final Button btnPlay = (Button) dialog.findViewById(R.id.btn_play);
		final Button btnStop = (Button) dialog.findViewById(R.id.btn_parar);
		final Button btnGravar = (Button) dialog.findViewById(R.id.btn_gravar);
		Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
		
		btnGravar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				recordAudio.recordAudio();
				btnGravar.setEnabled(false);
				btnStop.setEnabled(true);
				btnPlay.setEnabled(false);
			}
		});
		
		btnStop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				recordAudio.stopRecording();
				btnGravar.setEnabled(true);
				btnStop.setEnabled(false);
				btnPlay.setEnabled(true);
				listaArquivosUsuario.add(recordAudio.obterAudioRecorded());
			}
		});
		
		btnPlay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				recordAudio.play();
			}
		});
		
		btnOk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				if(recordAudio.obterAudioRecorded() != null){
					textViewAudio.setText(recordAudio.obterAudioRecorded().getName());
					textViewAudio.setVisibility(View.VISIBLE);
					imageViewImage.setVisibility(View.GONE);
				}
			}
		});
		
		dialog.show();
		
	}
	
	private void takePictureFromGallery(){
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
         
        startActivityForResult(intent, REQUEST_LOAD_IMAGE);
	}
	
	@SuppressWarnings("deprecation")
	private String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaColumns.DATA };
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
	private void btnClicks(){
		
		btnImagem.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createDialogImage();
			}
		});
		
		btnAudio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createDialogAudio();
			}
		});
		
		textViewAudio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				recordAudio.play();
			}
		});
		
		btnVideo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				takeVideo();
			}
		});
		
		btnSend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(occurrence != null){
					if(oType  == 6) {
						occurrence.setOtherType(oType);
						occurrence.setoType(0);
					} else {
						occurrence.setoType(oType);
						occurrence.setOtherType(0);
					}
				}
				
				String fileAnexo = null;
				File fileToDecode = null;
				
				try {
					
					if(listaArquivosUsuario.size() == 0) {
						Toast.makeText(getApplicationContext(), R.string.occurrence_adicionar_anexo, Toast.LENGTH_LONG).show();
						return;
					}
					fileToDecode = listaArquivosUsuario.get(listaArquivosUsuario.size() - 1);
					byte[] data = FileUtils.readFileToByteArray(fileToDecode);
					
					fileAnexo = Base64.encodeToString(data, Base64.NO_WRAP);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				mimeType = Utils.getMimeType(fileToDecode);
				assets.setAttachment(fileAnexo);				
				assets.setMimeType(mimeType);
				assets.setName(fileToDecode.getName());
				
				occurrence.setAssets(assets);
				occurrence.setOdate(occurrence_date.getHint().toString() + " " +
						occurrence_hour.getHint().toString().trim() );
				occurrenceMessage.setOccurence(occurrence);
				
				System.out.println(occurrence.getAddress() + ", obs = "
						+ occurrence.getObs() + ", lat = "
						+ occurrence.getLatitude() + ", long = "
						+ occurrence.getLongitude() + ", otherType = "
						+ occurrence.getOtherType() + ", oType = "
						+ occurrence.getoType()
						+ ", mime = " + occurrence.getAssets().getMimeType()
						+ ", name = " + occurrence.getAssets().getName());
				
				ProgressDialog progressDialog;
				progressDialog = ProgressDialog.show(
						Occurrence_Activity.this, null, "Enviando...",
						false, false);
			   
			   cadastrarOcorrencia(progressDialog);
			}
		});
	}
	
	private int obterTipo(){
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		return bundle.getInt("type");
	}
	
	@Background
	protected void cadastrarOcorrencia(ProgressDialog progressDialog) {

		try {
			
			OccurrenceResponse occurenceResponse = apiEmergencyWSClient.insertOccurence(occurrenceMessage, token);
			if (occurenceResponse.isSuccess()) {
				System.out.println(R.string.occurrence_sucesso);
				System.out.println(occurrence.getAddress() + ", obs = "
						+ occurrence.getObs() + ", lat = "
						+ occurrence.getLatitude() + ", long = "
						+ occurrence.getLongitude() + ", otherType = "
						+ occurrence.getOtherType() + ", oType = "
						+ occurrence.getoType()
						// +" "+ occurrence.getAssets().getAttachment()
						+ ", mime = " + occurrence.getAssets().getMimeType()
						+ ", name = " + occurrence.getAssets().getName());
				progressDialog.dismiss();
				Intent intent = new Intent(getApplicationContext(), Home.class);
				startActivity(intent);
				ToastSuccess();
				
			}
		} catch (HttpClientErrorException e) {
			e.getStatusCode();
			progressDialog.dismiss();
			ToastException();
		}

		catch (Exception e) {
			e.printStackTrace();
			progressDialog.dismiss();
			ToastException();
		}
	}
	
	@UiThread
	public void ToastSuccess() {
		Toast.makeText(getApplication(), R.string.occurrence_sucesso, Toast.LENGTH_LONG).show();
	}
	
	@UiThread
	public void ToastException() {
		Toast.makeText(getApplication(), R.string.occurrence_error, Toast.LENGTH_LONG).show();
	}
	
	private String obterToken(){
		SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, 0);
		return sharedPreferences.getString("token", null);
		
	}

	@Override
	public boolean onMenuItemClick(android.view.MenuItem arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}